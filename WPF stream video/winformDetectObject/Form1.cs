﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using Alturos.Yolo;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.Util;

namespace winformDetectObject
{
    public partial class Form1 : Form
    {
        Capture capture;
        public Form1()
        {
            InitializeComponent();
        }
        void LoadVideo()
        {
            if (capture == null)
            {
                OpenFileDialog ofd = new OpenFileDialog();
                ofd.Filter = "Video mp4|*.mp4";
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    capture = new Capture(ofd.FileName);
                }
            }
            capture.ImageGrabbed += Capture_ImageGrabbed;
            capture.Start();
        }

        private void Capture_ImageGrabbed(object sender, EventArgs e)
        {
            try
            {
                Mat m = new Mat();
                capture.Retrieve(m);
                //pictureBox1.Image = m.ToImage<Bgr, byte>().Bitmap;
                //Detect(m.ToImage<Bgr, byte>());
                DetectVehicel(m.ToImage<Bgr, byte>());
                Thread.Sleep((int)capture.GetCaptureProperty(Emgu.CV.CvEnum.CapProp.Fps));
            }
            catch (Exception)
            {

            }
        }
        private void Detect(Image<Bgr,byte> bimap)
        {
            //Random rand = new Random();
            
            //Rectangle rect = new Rectangle(rand.Next(50, 100), rand.Next(50, 100), 100, 200);
            //CvInvoke.Rectangle(bimap, rect,new MCvScalar(0,0,255), 2);
            //pictureBox1.Image = bimap.Bitmap;

            var configurationDetector = new YoloConfigurationDetector();
            var config = configurationDetector.Detect();
            using (var yoloWrapper = new YoloWrapper(config))
            {
                using(MemoryStream ms = new MemoryStream())
                {
                    bimap.Bitmap.Save(ms, ImageFormat.Png);
                    var items = yoloWrapper.Detect(ms.ToArray());
                    foreach (var item in items)
                    {
                        Rectangle rect = new Rectangle(item.X, item.Y,item.Width,item.Height);
                        CvInvoke.Rectangle(bimap, rect, new MCvScalar(0, 0, 255), 2);
                    }
                    pictureBox1.Image = bimap.Bitmap;

                }
                //items[0].Type -> "Person, Car, ..."
                //items[0].Confidence -> 0.0 (low) -> 1.0 (high)
                //items[0].X -> bounding box
                //items[0].Y -> bounding box
                //items[0].Width -> bounding box
                //items[0].Height -> bounding box
            }
        } 
        private void DetectVehicel(Image<Bgr, byte> img)
        {
            Image<Gray, byte> imgSmooth = img.SmoothGaussian(1).Convert<Gray, byte>().ThresholdBinaryInv(new Gray(50), new Gray(255));
            VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint();
            Mat m = new Mat();
            CvInvoke.FindContours(imgSmooth, contours, m, Emgu.CV.CvEnum.RetrType.External, Emgu.CV.CvEnum.ChainApproxMethod.ChainApproxSimple);
            for (int i = 0; i < contours.Size ; i++)
            {
                double premimeter = CvInvoke.ArcLength(contours[i],true);
                VectorOfPoint approx = new VectorOfPoint();
                CvInvoke.ApproxPolyDP(contours[i], approx, 0.04*premimeter,true);
                CvInvoke.DrawContours(img, contours, i, new MCvScalar(255, 0, 255),2);
                Rectangle brect = CvInvoke.BoundingRectangle(contours[i]);
                double ar = brect.Width / brect.Height;
                if (brect.Width > 50
                    //&& brect.Width<100
                    && brect.Height > 50
                    //&& brect.Height < 100
                    ) {
                    CvInvoke.Rectangle(img, brect, new MCvScalar(0, 0, 255), 2);

                    //moments center of the shape
                    var moments = CvInvoke.Moments(contours[i]);
                    int x = (int)(moments.M10 / moments.M00);
                    int y = (int)(moments.M01 / moments.M00);
                    if (approx.Size >= 5)
                    {
                        CvInvoke.PutText(img, "Car", new Point(x, y),
                            Emgu.CV.CvEnum.FontFace.HersheySimplex, 0.5, new MCvScalar(0, 0, 255));
                    }
                }
            }
            pictureBox1.Image = img.Bitmap;

        }
        void DetectText(Image<Bgr, byte> img)
        {
            Image<Gray, byte> sobel = img.Convert<Gray,byte>().Sobel(1, 0, 3).AbsDiff(new Gray(0.0)).Convert<Gray, byte>().ThresholdBinary(new Gray(100), new Gray(255));
            Mat SE = CvInvoke.GetStructuringElement(Emgu.CV.CvEnum.ElementShape.Ellipse, new Size(10, 1), new Point(-1, -1));
            sobel = sobel.MorphologyEx(Emgu.CV.CvEnum.MorphOp.Dilate, SE, new Point(-1, -1), 1, Emgu.CV.CvEnum.BorderType.Reflect, new MCvScalar(255));
            VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint();
            Mat m = new Mat();
            CvInvoke.FindContours(sobel, contours, m, Emgu.CV.CvEnum.RetrType.External, Emgu.CV.CvEnum.ChainApproxMethod.ChainApproxSimple);
            List<Rectangle> lst = new List<Rectangle>();
            for(int i=0;i< contours.Size; i++)
            {
                Rectangle brect = CvInvoke.BoundingRectangle(contours[i]);
                double ar = brect.Width / brect.Height;
                if (brect.Width > 30 && brect.Height > 30)
                    lst.Add(brect);
            }
            foreach (var item in lst)
            {
                CvInvoke.Rectangle(img, item, new MCvScalar(0, 0, 255), 2);
            }
           // pictureBox1.Image = img.Bitmap; 
            pictureBox1.Image = sobel.Bitmap; 
        }
        private void btn_Start_Click(object sender, EventArgs e)
        {
             capture.Start();
        }

        private void btn_Pause_Click(object sender, EventArgs e)
        {
            if (capture == null)
                return;
        }

        private void btn_Stop_Click(object sender, EventArgs e)
        {
            if (capture != null)
                capture.Stop();
        }

        private void btn_Brows_Click(object sender, EventArgs e)
        {
            LoadVideo();
        }
    }
    
}
