﻿using AForge.Video;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using AForge.Video.FFMPEG;
using AForge.Video.DirectShow;

namespace Detect
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        FileVideoSource stream;
        //MJPEGStream stream;
        public MainWindow()
        {
            InitializeComponent();
            stream = new FileVideoSource(@"C:\Users\Admin\Desktop\IMG\TEST\EZVZ0008.MP4");
            //stream = new MJPEGStream("rtsp://admin:HYBLMR@192.168.1.103:554/H.264");
            stream.NewFrame += new NewFrameEventHandler(Stream_NewFrame);
            stream.Start();
        }

        public BitmapImage ToBitmapImage(Bitmap bitmap)
        {
            BitmapImage bi = new BitmapImage();
            bi.BeginInit();
            MemoryStream ms = new MemoryStream();
            bitmap.Save(ms, ImageFormat.Bmp);
            ms.Seek(0, SeekOrigin.Begin);
            bi.StreamSource = ms;
            bi.EndInit();
            return bi;
        }
        void Stream_NewFrame(object sender, NewFrameEventArgs eventArgs)
        {
            BitmapImage bi;
            using (var bitmap = (Bitmap)eventArgs.Frame.Clone())
            {
                bi = ToBitmapImage(bitmap);
            }
            bi.Freeze(); // avoid cross thread operations and prevents leaks
            Dispatcher.BeginInvoke(new ThreadStart(delegate { videoPlayer.Source = bi; }));
        }

        private void Start_Click(object sender, RoutedEventArgs e)
        {
            stream.Start();
        }

        private void Stop_Click(object sender, RoutedEventArgs e)
        {
            stream.Stop();
        }

        void loadVideo()
        {
           
        }
    }
}
